/**
 * 
 */
package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Administrator
 *
 */
@RestController
public class HelloWorldController {

	@RequestMapping("/hello")
	public String hello() {
		//return "Hello World!";
		return "只争朝夕 不负韶华, 中国加油!";
	}
	
}
