FROM java:latest
MAINTAINER iogroup <iogroup@163.com>

# RUN mvn clean package -Dmaven.test.skip=true
RUN rm -rf /etc/localtime && ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} /app/app.jar

EXPOSE 8080

ENTRYPOINT ["nohup", "java","-jar","/app/app.jar","&"]